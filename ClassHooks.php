<?php

namespace WPezSuite\WPezClasses\ThemeHeadCleanup;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

class ClassHooks {

	protected $_new_component;
	protected $_arr_hook_defaults;
	protected $_arr_actions;
	protected $_arr_filters;

	public function __construct( InterfaceThemeHeadCleanup $obj ) {

		$this->_new_component = $obj;

		$this->setPropertyDefaults();

	}

	protected function setPropertyDefaults() {

		$this->_arr_hook_defaults = [
			'active'        => true,
			'component'     => $this->_new_component,
			'priority'      => '10',
			'accepted_args' => '1'
		];

		// actions
		$this->_arr_actions = [];

		$this->_arr_actions['init'] = [
			'hook'     => 'init',
			'callback' => 'cleanupHead',
			'priority' => 100
		];

		// filters
		$this->_arr_filters = [];

		$this->_arr_filters['style_loader_src'] = [
			'hook'     => 'style_loader_src',
			'callback' => 'filterStyleLoaderSrc',
			'priority' => 9999
		];

		$this->_arr_filters['script_loader_src'] = [
			'hook'     => 'script_loader_src',
			'callback' => 'filterStyleLoaderSrc',
			'priority' => 9999
		];

		$this->_arr_filters['emoji_svg_url'] = [
			'hook'     => 'emoji_svg_url',
			'callback' => 'filterEmojiSvgUrl',
			'priority' => 9999
		];

		$this->_arr_filters['tiny_mce_plugins'] = [
			'hook'     => 'tiny_mce_plugins',
			'callback' => 'filterTinyMMCEPlugins',
			'priority' => 9999
		];

	}


	/**
	 * @param bool $arr
	 *
	 * @return bool
	 */
	public function updateHookDefaults( $arr = false ) {

		if ( is_array( $arr ) ) {

			$this->_arr_hook_defaults = array_merge( $this->_arr_hook_defaults, $arr );

			return true;

		}

		return false;
	}

	/**
	 * @return mixed
	 */
	public function getActions() {

		return $this->_arr_actions;
	}


	/**
	 * @param bool $arr
	 *
	 * @return bool
	 */
	public function updateActions( $arr = false ) {

		if ( is_array( $arr ) ) {

			$this->_arr_actions = array_merge( $this->_arr_actions, $arr );

			return true;

		}

		return false;
	}

	/**
	 * @return mixed
	 */
	public function getFilters() {

		return $this->_arr_filters;
	}


	/**
	 * @param bool $arr
	 *
	 * @return bool
	 */
	public function updateFilters( $arr = false ) {

		if ( is_array( $arr ) ) {

			$this->_arr_filters = array_merge( $this->_arr_filters, $arr );

			return true;

		}

		return false;
	}


	protected function registerMaster( $arr_exclude, $arr_hooks, $str_wpfn = 'add_action' ) {


		foreach ( $arr_hooks as $str_ndx => $arr_hook ) {

			if ( in_array( $str_ndx, $arr_exclude ) ) {
				continue;
			}

			$arr = array_merge( $this->_arr_hook_defaults, $arr_hook );
			if ( $arr['active'] === false ) {
				continue;
			}

			$str_wpfn(
				$arr['hook'],
				[ $arr['component'], $arr['callback'] ],
				$arr['priority'],
				$arr['accepted_args']
			);
		}
	}


	/**
	 * @param bool $arr_exclude
	 */
	public function register( $arr_exclude = false ) {

		if ( ! is_array( $arr_exclude ) ) {
			$arr_exclude = [];
		}

		if ( ! empty( $this->_arr_actions ) ) {

			$this->registerMaster( $arr_exclude, $this->_arr_actions, 'add_action' );

		}

		if ( ! empty( $this->_arr_filters ) ) {

			$this->registerMaster( $arr_exclude, $this->_arr_filters, 'add_filter' );

		}

	}

}