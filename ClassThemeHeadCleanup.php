<?php

namespace WPezSuite\WPezClasses\ThemeHeadCleanup;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

if ( ! class_exists( 'ClassThemeHeadCleanup' ) ) {
	class ClassThemeHeadCleanup implements InterfaceThemeHeadCleanup {

		protected $_bool_ver_modify;
		protected $_bool_ver_remove;
		protected $_bool_emoji_svg_url;
		protected $_bool_tiny_mce_plugins;
		protected $_str_hash_algo;
		protected $_hash_date;
		protected $_arr_cleanup_tasks;

		public function __construct() {

			$this->setPropertyDefaults();

		}

		protected function setPropertyDefaults() {

			$this->_bool_ver_modify       = false;
			$this->_bool_ver_remove       = false;
			$this->_bool_emoji_svg_url    = true;
			$this->_bool_tiny_mce_plugins = false;
			$this->_str_hash_algo         = 'md5'; // http://php.net/manual/en/function.hash.php
			$this->_hash_date             = "Y";

			$this->_arr_cleanup_tasks = [

				// == set a
				'rsd_link'                        => true,
				'wlwmanifest_link'                => true,
				'index_rel_link'                  => true,
				// WP version
				'wp_generator'                    => true,

				// == set b
				'parent_post_rel_link'            => true,
				'start_post_rel_link'             => true,
				'adjacent_posts_rel_link'         => true,
				'adjacent_posts_rel_link_wp_head' => true,
				'wp_shortlink_wp_head'            => true,
				'rest_output_link_wp_head'        => true,
				'wp_oembed_add_discovery_links'   => true,

				// -- set c
				'feed_links'                      => true,

				// - set d
				'feed_links_extra'                => true,

				// - set e
				'rest_output_link_header'         => true,

				// set oth
				'wp_ver_modify'                   => true,
				'wp_ver_remove'                   => false,
				'emoji_svg_url'                   => true,
				'tiny_mce_plugins'                => true,
				'print_emoji_styles'              => true,
				'print_emoji_detection_script'    => true,
				'wp_staticize_emoji_for_email'    => true,
				'staticize_emoji'                 => true
			];


		}


		public function setTask( $str_task = '', $bool = false ) {

			if ( isset( $this->_arr_cleanup_tasks[ $str_task ] ) ) {

				$this->_arr_cleanup_tasks[ $str_task ] = (boolean) $bool;

				return true;
			}

			return false;
		}


		public function updateTasks( $arr = false ) {

			if ( is_array( $arr ) ) {
				$this->_arr_cleanup_tasks = array_merge( $this->_arr_cleanup_tasks, $arr );

				return true;
			}

			return false;
		}


		/**
		 * This is where the magic happens.
		 */
		public function cleanupHead() {

			foreach ( $this->_arr_cleanup_tasks as $str_task => $bool_var ) {

				if ( $bool_var === false ) {
					continue;
				}

				$str_task = strtolower( $str_task );
				switch ( $str_task ) {

					// == set a
					case 'rsd_link':
					case 'wlwmanifest_link':
					case 'index_rel_link':
						// WP version
					case 'wp_generator':
						remove_action( 'wp_head', $str_task );
						break;

					// == set b
					case 'parent_post_rel_link':
					case 'start_post_rel_link':
					case 'adjacent_posts_rel_link':
					case 'adjacent_posts_rel_link_wp_head':
					case 'wp_shortlink_wp_head':
					case 'rest_output_link_wp_head':
					case 'wp_oembed_add_discovery_links':
						remove_action( 'wp_head', $str_task, 10);
						break;

					// set e
					case 'rest_output_link_header':
						remove_action( 'template_redirect', $str_task, 11);
						break;


					// == set c
					case 'feed_links':
						remove_action( 'wp_head', $str_task, 2 );
						break;

					// == set d
					case 'feed_links_extra':
						remove_action( 'wp_head', $str_task, 3 );
						break;

					// == set oth
					case 'wp_ver_modify':
						$this->_bool_ver_modify = true;
						$this->_bool_ver_remove = false;
						break;

					// == set oth
					case 'wp_ver_remove':
						$this->_bool_ver_modify = false;
						$this->_bool_ver_remove = true;
						break;

					case 'emoji_svg_url':
						$this->_bool_emoji_svg_url = false;
						break;

					case 'tiny_mce_plugins':
						$this->_bool_tiny_mce_plugins = true;
						break;

					case 'print_emoji_styles':
						remove_action( 'admin_print_styles', $str_task );
						remove_action( 'wp_print_styles', $str_task );
						break;

					case 'print_emoji_detection_script':
						remove_action( 'wp_head', $str_task, 7 );
						remove_action( 'admin_print_scripts', $str_task );
						break;

					case 'wp_staticize_emoji_for_email':
						remove_filter( 'wp_mail', $str_task );
						break;

					case 'staticize_emoji':
						remove_filter( 'the_content_feed', $str_task );
						remove_filter( 'comment_text_rss', $str_task );
						break;


					default:
						'TODO';

				}

			}
		}


		// https://wordpress.stackexchange.com/questions/185577/disable-emojicons-introduced-with-wp-4-2/185578#185578
		public function filterEmojiSvgUrl() {

			return $this->_bool_emoji_svg_url;
		}


		public function filterTinyMMCEPlugins( $plugins ) {

			if ( $this->_bool_tiny_mce_plugins === true ) {
				if ( is_array( $plugins ) ) {
					return array_diff( $plugins, [ 'wpemoji' ] );
				} else {
					return [];
				}
			}

			return $plugins;
		}


		/**
		 * rather than totally remove the version (which helps the browser) let's just make it a bit more difficult to ID
		 */
		public function filterStyleLoaderSrc( $str_src ) {

			if ( $this->_bool_ver_modify === false && $this->_bool_ver_remove === false ) {
				return $str_src;
			}

			if ( strpos( $str_src, 'ver=' ) && strpos( $str_src, '/wp-includes/' ) ) {

				if ( $this->_bool_ver_remove === true ) {
					// TODO esc_url?
					return remove_query_arg( 'ver', $str_src );
				}
				// TODO esc_url?
				// we'll hash the year so the src ver is still browser cache friendly
				$str_src = str_replace( 'ver=', 'ver=' . hash( $this->_str_hash_algo, date( $this->_hash_date ) ) . '-', $str_src );
			}

			return $str_src;
		}

	}
}