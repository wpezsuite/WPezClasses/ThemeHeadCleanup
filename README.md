## WPezClasses: Theme Head Cleanup

__Your theme's \<head\> is "dirty", now you can clean it up The ezWay.__
   
No need for a plugin ma.   
   

> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Simple Example



_Recommended: Use the WPezClasses autoloader (link below)._


    use WPezSuite\WPezClasses\ThemeHeadCleanup\ClassThemeHeadCleanup as THC
    use WPezSuite\WPezClasses\hemeHeadCleanup\ClassHooks as Hooks
    
    $new_thc = new THC();
    // $new_thc->setTask(...);
    // $new_thc->updateTasks(...);
    $new_hooks = new Hooks($new_thc);
    $new_hooks->register();
    
    
    


There are two public methods for customizing what does or does not get cleaned up:

- setTask($str_task, $bool ) - Update a single task. $bool = false will stop the clean up task from being done.

- updateTasks( $arr ) - Updates the tasks in bulk with an array.

You can find the list of tasks in the method setPropertyDefaults().

### FAQ

__1) Why?__

It's a fairly common request / need, and typically "set it and forget it." This make it ez.

__2) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 


 __3) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 



### HELPFUL LINKS

- https://crunchify.com/how-to-clean-up-wordpress-header-section-without-any-plugin/

- https://www.wpexplorer.com/clean-wordpress-head/

- https://webdesignviews.com/clean-up-wordpress-header/

- https://jeremyhixon.com/cleaning-up-wordpress-head/

- https://wordpress.stackexchange.com/questions/185577/disable-emojicons-introduced-with-wp-4-2/185578#185578




### TODO

- 


### CHANGE LOG

- v0.0.3 - Monday 22 April 2019
    - Updated: interface file / name

- v0.0.2 - Tuesday 16 April 2019
    - Updated - Hooks to use (new) hooks boilerplate

- v0.0.1 - Monday 15 April 2019
    - This has been in development for quite some time, but not independently repo'ed until now. Please pardon the delay. 