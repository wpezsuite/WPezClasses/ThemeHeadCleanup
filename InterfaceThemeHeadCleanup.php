<?php

namespace WPezSuite\WPezClasses\ThemeHeadCleanup;

interface InterfaceThemeHeadCleanup {

	public function cleanupHead();

	public function filterEmojiSvgUrl();

	public function filterTinyMMCEPlugins( $plugins );

	public function filterStyleLoaderSrc( $str_src );


}